/*  Tool to compute the RDF for particle laden flows
 *               compile using "gcc -o rdf rdf.c -lm"
 *               Usage ./rdf -fnf example.xyz -N [NB bins] -L [dom size]
 *   Modified from: Cameron F. Abrams
 *                               */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


/* Prints usage information */
void usage ( void ) {
  fprintf(stdout,"rdf usage:\n");
  fprintf(stdout,"rdf [options]\n\n");
  fprintf(stdout,"Options:\n");
  fprintf(stdout,"\t -fnf [string]\t\tFile name format (i.xyz)\n");
  fprintf(stdout,"\t -N [real]\t\tNumber of bins\n");
  fprintf(stdout,"\t -L [real]\t\tMaximum length side of cube\n");
  fprintf(stdout,"\t -h           \t\tPrint this info.\n");
}

/* Reads in a configuration snapshot in XYZ format (such
 *    as that created by mdlj.c) */
int xyz_in (FILE * fp, double * rx, double * ry, double * rz,
	     int * N) {
  int i;
  int has_vel;
  char dum[2];
  double vx,vy,vz;

  for (i=0;i<(*N);i++) {
    /*fscanf(fp,"%s%lf%lf%lf",&dum,&rx[i],&ry[i],&rz[i]);*/
    fscanf(fp,"%lf%lf%lf",&rx[i],&ry[i],&rz[i]);
  }
  return has_vel;
}

/* An N^2 algorithm for computing interparticle separations
 *    and updating the radial distribution function histogram.
 *       Implements parts of Algorithm 7 in F&S. */
void update_hist ( double * rx, double * ry, double * rz,
		   int N, double L, double rc2, double dr, int * H ) {
   int i,j,it=0;
   double dx, dy, dz, r2;
   int bin;
   double hL = L;
   for (i=0;i<N;i++) {
     fprintf(stdout,"%.4lf \n",(double)(i*100)/(double)(N));
     for (j=0;j<N;j++) {
      if (i!=j){
       dx  = fmin(pow((rx[i]-rx[j]),2),pow((rx[i]-rx[j]+L),2));
       dy  = fmin(pow((ry[i]-ry[j]),2),pow((ry[i]-ry[j]+L),2));
       dz  = fmin(pow((rz[i]-rz[j]),2),pow((rz[i]-rz[j]+L),2));
       r2 = sqrt(dx + dy + dz);
       if (r2<L/2) {
	       bin=(int)(r2/dr);
	       H[bin]+=1;
         /*fprintf(stdout,"%lf\n",r2);*/
         it++;
       }
      }
     }
   }
}

int main ( int argc, char * argv[] ) {

  double * rx, * ry, * rz;
  int N=216, nB=0;
  int * H;
  double L=0.0, V=0.0;
  double dr=0.1, r, vb, nid;
  double Lcube = 6.28,  nbReal=200.0, Htot=0.0;
  int i;
  long int lines =0;

  char * fnf;
  FILE * fp;

  char fn[35];

  /* Here we parse the command line arguments;  If
 *    you add an option, document it in the usage() function! */
  for (i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-N")) nbReal=atof(argv[++i]);
    else if (!strcmp(argv[i],"-L")) Lcube=atof(argv[++i]);
    else if (!strcmp(argv[i],"-fnf")) fnf=argv[++i];
    else if (!strcmp(argv[i],"-h")) {
      usage(); exit(0);
    }
    else {
      fprintf(stderr,"Error: Command-line argument '%s' not recognized.\n",
	      argv[i]);
      exit(-1);
    }
  }

  /* compute the number of bins, and allocate the histogram */
  nB = (int)(nbReal) ;
  dr = Lcube/(nbReal-1);
  H = (int*)calloc(nB,sizeof(int));

  /* Generate the file name */
  sprintf(fn,fnf,i);
  /* Open the file */
  fp=fopen(fn,"r");

  while (EOF != (fscanf(fp, "%*[^\n]"), fscanf(fp,"%*c")))
          ++lines;
  N=lines-2;
  fclose(fp);

  /* Allocate the position arrays */
  rx = (double*)malloc(N*sizeof(double));
  ry = (double*)malloc(N*sizeof(double));
  rz = (double*)malloc(N*sizeof(double));

  fp=fopen(fn,"r");
  /* If the file is there... */
  if (fp) {
    /* Read in the data, ignoring velocities */
      xyz_in(fp,rx,ry,rz,&N);
      /* Close the file */
      fclose(fp);

      /* Update the histogram by sampling the configuration */
      update_hist(rx,ry,rz,N,Lcube,Lcube,dr,H);
      fprintf(stderr,"# %s...\n",fn);
    }

  /* Normalize and output g(r) to the terminal */
  for (i=0;i<nB;i++) {
    Htot+=H[i];}
  for (i=0;i<nB;i++) {
    fprintf(stdout,"%.4lf %.4lf %lf\n",i*dr,(double)(H[i]),(Htot));
  }
}
