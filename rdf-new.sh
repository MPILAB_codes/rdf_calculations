#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=40
#SBATCH --time=3:00:00
#SBATCH --job-name=bc1_10krdf
#SBATCH --output=bc11_10krdf.txt
#SBATCH --mail-user=asaieed@uwaterloo.ca
#SBATCH --mail-type=ALL

cd $SLURM_SUBMIT_DIR
export PENCIL_HOME=$HOME/pencil-code

LOGFILE="logrdf"
module load NiaEnv/2019b
module load gcc/9.2.0
module load openmpi/4.0.3

./rdf -fnf bc1_10k.xyz >>$LOGFILE 2>&1

